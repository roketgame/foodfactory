﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;


namespace FoodFactory
{
    public class SceneCont : CoreSceneCont
    {

        public List<GameObject> ProductPrefabs;
        public List<BaseProduct> listProductTemps;
        public Dictionary<ProductCategory, List<BaseProduct>> listProductsCache;
        public override void Init()
        {



            base.Init();



        }
        public override void StartGame()
        {

            listProductTemps = new List<BaseProduct>();
            listProductsCache = new Dictionary<ProductCategory, List<BaseProduct>>();
            foreach (GameObject productGo in ProductPrefabs)
            {
                BaseProduct product = SpawnItem<BaseProduct>(productGo, this.transform.position, null, this.transform.root, false);
                product.enabled = false;
                product.gameObject.SetActive(false);
                listProductTemps.Add(product);
                listProductsCache.Add(product.Category, new List<BaseProduct>());
            }
            //for (int i = 0; i < 20; i++)
            //{
            //    spawnProduct(ProductCategory.FRIED);
            //}
            base.StartGame();
        }


        protected override void onItemSpawned(CoreSceneObject _sceneObject)
        {
            base.onItemSpawned(_sceneObject);
        }

        public override void Update()
        {
            base.Update();


        }

        public BaseProduct CreateProduct(ProductCategory _category, Transform _parent, Vector3 _position)
        {

            BaseProduct product = addProduct(_category);
            Vector3 pos = _position;
            //pos.y += (product.GetSize().y);
            product.transform.position = pos;
            product.transform.rotation = Quaternion.identity;
            product.transform.parent = _parent;
            product.SpawnPosition = product.transform.position;
            product.SetEnableTrigger(true);
            product.StartGame();


            return product;
        }
        private BaseProduct addProduct(ProductCategory _category)
        {
            BaseProduct product = null;

            List<BaseProduct> listCache = getCacheProducts(_category);

            if (listCache.Count > 0)
            {
                product = listCache[0];
                listCache.RemoveAt(0);
                listProductsCache[_category] = listCache;
            }
            else
            {
                product = spawnProduct(_category);
            }

            product.gameObject.SetActive(true);


            return product;
        }


        private BaseProduct spawnProduct(ProductCategory _category)
        {
            BaseProduct product = null;
            BaseProduct tempProduct = GetTempProduct(_category);
            if (tempProduct != null)
            {
                product = SpawnItem<BaseProduct>(tempProduct.gameObject, this.transform.position, null, this.transform.root, true);
                product.gameObject.SetActive(false);



            }
            return product;
        }

        public void RemoveProduct(BaseProduct _product)
        {
            if (_product)
            {
                if (addToCacheList(_product))
                {
                    _product.Reset();
                    _product.gameObject.SetActive(false);
                }

            }
        }


        private bool addToCacheList(BaseProduct _product)
        {
            bool r = false;
            if (_product)
            {
                if (!listProductsCache[_product.Category].Contains(_product))
                {
                    listProductsCache[_product.Category].Add(_product);
                    r = true;
                }


            }

            return r;
        }
        private bool removeFromCacheList(BaseProduct _product)
        {
            bool r = false;
            if (_product)
            {
                if (listProductsCache[_product.Category].Contains(_product))
                {
                    listProductsCache[_product.Category].Remove(_product);
                    r = true;
                }


            }

            return r;
        }


        private List<BaseProduct> getCacheProducts(ProductCategory _cat)
        {
            List<BaseProduct> r = null;
            if (listProductsCache.ContainsKey(_cat))
            {
                r = listProductsCache[_cat];
            }
            return r;
        }

        public BaseProduct GetTempProduct(ProductCategory _cat)
        {
            return listProductTemps.Find(temp => temp.Category == _cat);
        }



        public T GetItemByClass<T>() where T : CoreSceneObject
        {
            T result = null;
            List<T> list = GetItems<T>();
            if (list.Count > 0)
                result = list[0];


            return result;
        }


        //public override bool DestroyItem(BaseSceneObject _item, float _destroyDelay = 0)
        //{
        //    //if (_item)
        //    //{
        //    //    if (_item.GetComponent<FixedJoint>()) Destroy(_item.GetComponent<FixedJoint>());
        //    //    if (_item.rb) Destroy(_item.rb);

        //    //}
        //    //float destroyDelay = _destroyDelay > 0 ? calculateDestroyDelay() : 0;
        //    float destroyDelay = _destroyDelay > 0 ? 2 : 0;
        //    return base.DestroyItem(_item, destroyDelay);

        //}

        private float calculateDestroyDelay()
        {
            float r = 0;
            int totalItem = GetItems<CoreSceneObject>().Count;
            int maxCount = 1000;
            int maxDelay = 5;
            if (totalItem > 0 && totalItem < maxCount)
            {
                int stepCount = Mathf.FloorToInt(maxCount / maxDelay); //200
                int step = Mathf.CeilToInt((float)totalItem / (float)stepCount); //1-2-3-4
                r = Mathf.FloorToInt(maxDelay - step);
            }

            r = Mathf.Max(r, maxDelay);
            //Debug.Log("totalitem " + totalItem + " r : " + r);
            return r;

        }
    }

}