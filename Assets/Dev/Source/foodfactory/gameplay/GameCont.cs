﻿using FoodFactory;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using RoketGame;


namespace FoodFactory
{
    [System.Serializable]
    public class Config
    {
        public static Config Instance { get { return ((GameCont)CoreGameCont.Instance).config; } }
        public int MACHINES_START_LEVEL = 1;
        //public int MACHINES_START_DURATION = 4;
        //public int MACHINES_START_LEVELCOST = 10;
        //public int DURATION_FACTOR_LEVEL = 2; //MULTIPLY
        //public int DURATION_FACTOR_SUBLEVEL = 2; //MULTIPLY

    }
        public class GameCont : CoreGameCont
    {
        public IdleController IdleCont;
        public bool IsEnableAllUpgradeButtons = false;
        private List<IProduction> listProductions;
        public Config config;

        public override void Init()
        {
            


            base.Init();

            MovingLine.Instance.Init();

            IdleCont = gameObject.AddComponent<IdleController>();
            IdleCont.Init(0);
            IdleCont.TotalMoney.Value = 500;
            IdleCont.MoneyEvent.AddListener(onEventIdleMoney);
            //idleCont.AddProduction(machinePeeler);
            //machinePeeler.ValueTime.EventOnUpdated.AddListener(idleTimerHandler);
            listProductions = new List<IProduction>();
            foreach (CoreObject baseObj in FindObjectsOfType<CoreObject>())
            {
                if (baseObj is IProduction)
                {
                    IProduction production = (IProduction)baseObj;
                    if (production != null)
                    {
                        IdleCont.AddProduction(production.GetIdlePro());
                        listProductions.Add(production);
                    }
                }


            }

         

            uiCont.EventTap.AddListener(onUiTouchHandler);
            //uiCont.OpenPage("Page0");
            uiCont.OpenPage<PageGame>();







            

            //{
            //    IdleProduction peeler = GetIdleProductionByName("peeler");
            //    MachineFactor factorDuration = new MachineFactor();
            //    factorDuration.LevelInterval = 100;
            //    factorDuration.FactorMultiply = 3;

            //    MachineIdle idle = new MachineIdle();
            //    idle.DurationFactorOfSubLevel = factorDuration;
            //    idle.Create("peeler", 1, 4);
            //}
           
            


            //if(GetIdleProductionByName("slicer"))
            GetIdleProductionByName("peeler").ValueRevenue.Value = 6;

           


            ((UICont)uiCont).StartIdleGame();


            UICont.Instance.GetPageByClass<PageGame>().SetMoney( IdleCont.TotalMoney.GetValueLabel());





            //idlePro.ValueRevenue.AddFactor(new IdleValueFactor(0, 100, 2, 1));
            //idlePro.ValueRevenue.AddFactor(new IdleValueFactor(100, 100, 2, ?));  
            //idlePro.ValueRevenue.AddFactor(new IdleValueFactor(100, 200, 2, ?));  

            //idlePro.ValueRevenue.AddFactor(new IdleValueFactor(0, 100, 2, bir_onceki_levela_ekle));
            //idlePro.ValueRevenue.AddFactor(new IdleValueFactor(100, 100, 3, carpan));
            //idlePro.ValueRevenue.AddFactor(new IdleValueFactor(100, 200, 4, bir_onceki_levela_ekle));
            //idlePro.ValueRevenue.AddFactor(new IdleValueFactor(200, 200, 3, carpan));
            //idlePro.ValueRevenue.AddFactor(new IdleValueFactor(0, 100, 2, bir_onceki_levela_ekle));
            //idlePro.ValueRevenue.AddFactor(new IdleValueFactor(100, 100, 3, carpan));
            //idlePro.ValueRevenue.AddFactor(new IdleValueFactor(100, 200, 4, bir_onceki_levela_ekle));
            //idlePro.ValueRevenue.AddFactor(new IdleValueFactor(200, 200, 3, carpan));
            //idlePro.ValueRevenue.AddFactor(new IdleValueFactor(0, 100, 2, bir_onceki_levela_ekle));
            //idlePro.ValueRevenue.AddFactor(new IdleValueFactor(100, 100, 3, carpan));
            //idlePro.ValueRevenue.AddFactor(new IdleValueFactor(100, 200, 4, bir_onceki_levela_ekle));
            //idlePro.ValueRevenue.AddFactor(new IdleValueFactor(200, 200, 3, carpan));













        }

        /* IDLE */
        private void onEventIdleMoney(BigFloat _totalMoney)
        {
            Debug.Log("GameCont.onEventIdleMoney : " + _totalMoney);
            //UICont.Instance.GetPageByClass<PageGame>().SetMoney(_totalMoney.ToString());
            UICont.Instance.GetPageByClass<PageGame>().RefreshMoney();
        }



        public IdleProduction GetIdleProductionByName(string _name)
        {
            IdleProduction idlePro = null;
            foreach (IProduction production in listProductions)
            {
                if (production.GetIdlePro().Id.ToLower() == _name)
                    idlePro = production.GetIdlePro();

            }
            return idlePro;
        }

        public IProduction GetProductionByName(string _name)
        {
            IProduction pro = null;
            foreach (IProduction production in listProductions)
            {
                //if( ((MonoBehaviour)production).name == _name)
                if (production.GetIdlePro().Id.ToLower() == _name.ToLower())
                    pro = production;

            }
            return pro ;
            ;
        }





        private void onUiTouchHandler(RoketGame.TouchUI tap)
        { 
            Debug.Log("GameCont.onUiTouchHandler " + tap.Target.Id);
            switch (tap.Target.Id)
            {
                case "PageGame/BtnAddMoney":
                    IdleCont.TotalMoney.Value += 500;
                    UICont.Instance.GetPageByClass<PageGame>().RefreshMoney();
                    break;
            }

        }


        //public override void Update()
        //{

        //}
        Vector3 Rotate90CW(Vector3 aDir, int section, float len)
        {
            Vector3 v = new Vector3(-aDir.z, 0, aDir.x).normalized;
            return (v * len);
        }
        // counter clockwise



        public override void StartGame()
        {
            base.StartGame();


            MovingLine.Instance.StartGame();
            IdleCont.StartIdle();
            return;
            for (int i = 0; i < 100; i++)
            {
                Platform randomPlatform = MovingLine.Instance.Debug_GetRandomPlatform();
                Vector3 pos = randomPlatform.GetRandomPos();




                BaseProduct product = ((SceneCont)SceneCont.Instance).CreateProduct(ProductCategory.RAW, MovingLine.Instance.transform, pos);
                if (randomPlatform)
                {
                    //product.AddJoint(randomPlatform);
                }

            }


            //UICont.Instance.GetPageByClass<PageGame>().BtnUpgrade.click
        }


        public override void GameOver(int _result)
        {
            base.GameOver(_result);

        }

        /* test */
        public void SetIsEnableAllUpgradeButtons()
        {
            IsEnableAllUpgradeButtons = !IsEnableAllUpgradeButtons;
        }

    }

}