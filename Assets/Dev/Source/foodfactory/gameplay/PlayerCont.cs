﻿using DigitalRuby.Tween;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;

namespace FoodFactory
{
    public struct CamStepData
    {
        public Vector3 LocalPos;
        //public Vector3 LocalRot;
        public Quaternion LocalRot;
        public float Size;
    }
    public class PlayerCont : CorePlayerCont
    {
        public Camera[] CamSteps;
        private List<CamStepData> listCamSteps;
        private float camStepAnimTime;
        public int currCamStep;
        public override void Init()
        {
            base.Init();
            CoreInputCont.Instance.EventSwipe.AddListener(onSwipe);
            listCamSteps = new List<CamStepData>();
            for (int i = 0; i < CamSteps.Length; i++)
            {
                Camera cam = CamSteps[i];
                CamStepData data;
                data.LocalPos = cam.transform.localPosition;
                data.LocalRot = cam.transform.localRotation;
                data.Size = cam.orthographicSize;
                listCamSteps.Add(data);
                if (i > 0)
                    Destroy(cam.gameObject);
            }

        }

        private void onSwipe(RoketGame.Touch _swipe)
        {

            float swipeMagnitude = (_swipe.GetPoint(1) - _swipe.GetPoint(0)).magnitude;
            //Debug.Log("swipe speed " + _swipe.GetSpeed());
            bool dirUp =(_swipe.GetNormal().y <= 0f);
            //int speed = Mathf.Clamp(Mathf.FloorToInt(_swipe.GetSpeed()), 1, 4);

            int nextStep = currCamStep;
            if (swipeMagnitude > 0.02f)
            {
                nextStep = currCamStep + ((dirUp ? 1 : -1) );
                if (!dirUp && _swipe.GetSpeed() > 2) //hizli home'a donus
                    nextStep = 0;
                Debug.Log("swipe swipeMagnitude " + swipeMagnitude);
                Debug.Log("swipe nextStep " + nextStep);
                nextStep = Mathf.Clamp(nextStep, 0, listCamSteps.Count);
                Debug.Log("swipe nextStepfinal " + nextStep);
                SetCamStep(nextStep);
            }

        }

        public void SetCamStep(int _index)
        {
            if(_index < listCamSteps.Count)
            {
                currCamStep = _index;
                SetCamStepTweenAnim();
            }
        }

        private void SetCamStepTweenAnim()
        {
            camStepAnimTime = 0;

            System.Action<ITween<float>> updateAnim = (t) =>
            {
                CamStepData targetData = listCamSteps[currCamStep];
                //if (targetData != null)
                {

                    Vector3 newPos = Vector3.Lerp(Cam.transform.localPosition, targetData.LocalPos, t.CurrentValue);
                    Quaternion newRot = Quaternion.Lerp(Cam.transform.localRotation, targetData.LocalRot, t.CurrentValue);
                    float newSize = Mathf.Lerp(Cam.orthographicSize, targetData.Size, t.CurrentValue);
                    Cam.transform.localPosition = newPos;
                    Cam.transform.localRotation = newRot;
                    Cam.orthographicSize = newSize;

                }
                //Cam.gameObject.
            };

          
            
              
                Cam.gameObject.Tween(Cam.gameObject.GetInstanceID(), camStepAnimTime, 1, 0.5f, TweenScaleFunctions.QuarticEaseInOut, updateAnim);

           

        }
    }
}