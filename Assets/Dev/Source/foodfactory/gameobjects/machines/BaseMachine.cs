﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;



namespace FoodFactory
{

    public class MachineFactor
    {
        public  IdleValue idleValue;
        //public int factorType;
        //public int totalLoop;
        //public int intervalInLoop;
        //public int levelCountInLoop;
        public int startLevel;
        public int endLevel;
        public float factor;
        //public float startFactor;
        //public float scaleOfFactor;


        public   MachineFactor(IdleValue _idleValue, int _factorType,  int _startLevel, int _endLevel, float _factor)
        {
            idleValue = _idleValue;
            factor = _factor;
            startLevel = _startLevel;
            endLevel = _endLevel;

           
            _idleValue.AddFactor(new IdleValueFactor(startLevel, endLevel, factor, _factorType));
               
            
        }

    }
    public class FactorHelper
    {
        /* idle factor info */
        //public float StartFactor;
        //public int IdleFactorType;
        //public IdleValue IdleValue;
        //public string Id;

        public string Id;
        private IdleValue idleValue;
        //private List<MachineFactor> listMainLevels;
        public List<MachineFactor> ListLevels;
        //public int totalMainLevel = 100;
        public int StepCount;
        //public int currLevel = 0;

        public  FactorHelper(string _id, IdleValue _idleValue,  int _stepCount, int _interval,  int _startLevel, int _levelCountPerLoop, float _startFactor, float _factorOfIdleFactor, int _idleFfactorType, bool _isMultiplyOperation = true  )
        {
            idleValue = _idleValue;
            StepCount = _stepCount;
            ListLevels = new List<MachineFactor>();
            Id = _id;
            //CreateIdleFactors(_idleFfactorType, _stepCount, _interval, _levelCountPerLoop, _startLevel, _startFactor, _scaleOfFactor);


            float factor = _startFactor;
            for (int i = 0; i < _stepCount; i++)
            {
                int startLevel = _startLevel + (i * _interval);
                int endLevel = startLevel + (_levelCountPerLoop - 1);

                MachineFactor machineFactor = new MachineFactor(idleValue, _idleFfactorType, startLevel, endLevel, factor);
                ListLevels.Add(machineFactor);
                Debug.Log("CreateIdleFactors -> " + Id + ", startLevel(" + startLevel + "), endLevel: (" + endLevel + "), factor: (" + factor + "), type: (" + _idleFfactorType + ")");
                if (_isMultiplyOperation)
                    factor *= _factorOfIdleFactor;
                else
                    factor += _factorOfIdleFactor;
            }
        }

        //public void CreateIdleFactors( int _factorType, int _totalLoop, int _intervalInLoop, int _levelCountInLoop, int _startLevel, float _startFactor, float _scaleOfFactor, bool __isMultiplyOperation)
        //{


        //    float factor = _startFactor;
        //    for (int i = 0; i < _totalLoop; i++)
        //    {
        //        int startLevel = _startLevel + (i * _intervalInLoop);
        //        int endLevel = startLevel + (_levelCountInLoop - 1);

        //        MachineFactor machineFactor = new MachineFactor(idleValue, _factorType, startLevel, endLevel, factor);
        //        ListLevels.Add(machineFactor);
        //        Debug.Log("CreateIdleFactors -> " + Id+ ", startLevel(" + startLevel + "), endLevel: (" + endLevel + "), factor: (" + factor + "), type: (" + _factorType + ")");
        //        factor *= _scaleOfFactor;
        //    }
        //}


        public float GetNextFactor(int _level)
        {
            float f = 0;
                for (int i = 0; i < ListLevels.Count; i++)
                {
                    if (_level < ListLevels[i].startLevel)
                    {
                        f = ListLevels[i].factor;
                        break;
                    }

                }



            return f;
        }




    }
    public class BaseMachine : SceneObject, IProduction
    {
        //public float TimerDelay = 0;
        public InputProcessor InputProc;
        public OutputProcessor OutputProc;
        public int InputUnitCount = 1;
        public int OutputUnitCount = 1;
        public int InputAmoutPerProcess = 1;
        public int OutputAmoutPerProcess = 1;
        public UpgradeWidget WidgetUpgrade;
        public TextMeshProUGUI WidgetText;
        private Timer timer;
        private IdleProduction idlePro;
        public FactorHelper FactorRevenueExtra;
        public FactorHelper FactorDurationExtra;
        //private Dictionary<string, FactorHelper> listMachineIdles;
        public bool IsLastMachine = false;
        public int LevelCountInStep = 10;
        public MoneyAnimation MoneyAnim;



        public override void Init()
        {
            base.Init();
           

            timer = new Timer();
            timer.Init();
            //listMachineIdles = new Dictionary<string, FactorHelper>();
            idlePro = new IdleProduction(name.ToLower(), 100, IsLastMachine);
            idlePro.ValueLevel.Value = 1;
        
            idlePro.ValueRevenue.Value = 1;
            idlePro.ValueRevenue.AddFactor(new IdleValueFactor(0, 1000, 1, 2));


            idlePro.ValueLevelCost.Value = 10;
            idlePro.ValueAmount.Value = 1;
            idlePro.ValueTotalMoney.Value = 1;


            //idlePro.ValueDuratiom.Value = 3f;
            idlePro.ValueDuratiom.Value = 0.1f;
            //idlePro.ValueDuratiom.Value = 0f;

            //idlePro.ValueDuratiom.AddFactor(new IdleValueFactor(0, 1, 0, 1));
            //idlePro.ValueDuratiom.AddFactor(new IdleValueFactor(1, 1, 3, 1));
            //idlePro.ValueDuratiom.AddFactor(new IdleValueFactor(2, 1000, 1, 2));
            idlePro.ValueDuratiom.AddFactor(new IdleValueFactor(0, 1000, 1, 2));



            //idlePro.ValueDuratiom.AddFactor(new IdleValueFactor(10, 10, -0.2f, 1));
            //idlePro.ValueDuratiom.AddFactor(new IdleValueFactor(20, 21, -0.2f, 1));
            //idlePro.ValueDuratiom.AddFactor(new IdleValueFactor(30, 31, -0.2f, 1));
            //idlePro.ValueDuratiom.AddFactor(new IdleValueFactor(40, 41, -0.2f, 1));
            //idlePro.ValueDuratiom.AddFactor(new IdleValueFactor(50, 51, -0.2f, 1));
            //idlePro.ValueDuratiom.AddFactor(new IdleValueFactor(60, 61, -0.2f, 1));
            //idlePro.ValueDuratiom.AddFactor(new IdleValueFactor(70, 71, -0.2f, 1));
            //idlePro.ValueDuratiom.AddFactor(new IdleValueFactor(80, 81, -0.2f, 1));
            //idlePro.ValueDuratiom.AddFactor(new IdleValueFactor(90, 91, -0.1f, 1));
            //idlePro.ValueDuratiom.AddFactor(new IdleValueFactor(100, 101, -0.1f, 1));


            //FactorRevenueExtra =  new FactorHelper(name+"/RevenueExtra", idlePro.ValueRevenue, 5, 20, 20, 1, 2, 3, 1);
            FactorDurationExtra = new FactorHelper(name + "/Duration", idlePro.ValueDuratiom, 1000, LevelCountInStep, 2, LevelCountInStep, -0.01f, 1, 1, true);
            //FactorDurationExtra = new FactorHelper(name + "/DurationExtra", idlePro.ValueDuratiom, 100, 10, 10, 1, -0.1f, 0, 1, false);

            //idlePro.ValueRevenue.AddFactor(new IdleValueFactor(0, 10, 1, 1));
            //idlePro.ValueRevenue.AddFactor(new IdleValueFactor(10, 10, 2, ?));  
            //idlePro.ValueRevenue.AddFactor(new IdleValueFactor(10, 20, 2, ?));  

            //level1: 1$  10 production -> 150$
            //level2: 2$  2 production -> 60$ + 150 = 210
            //level3: 3$
            //level4: 4$
            //level100: 100  
            //level100: 100 * 2 = 10 production x +(10 * 200) = x + 2000
            //level101: 2002
            //level101: 30060
            //level101: 30090
            //level101: 300


            idlePro.ValueLevelCost.AddFactor(new IdleValueFactor(1, 5, 1f, 2));
            idlePro.ValueLevelCost.AddFactor(new IdleValueFactor(6, 10, 2f, 2));
            idlePro.ValueLevelCost.AddFactor(new IdleValueFactor(11, 14, 3f, 2));
            idlePro.productionEvent.AddListener(onProductionTimerHandler);



        }

        /* idle */
        //public float GetNextFactor( string _machineIdleId)
        //{
        //    float f = 0;
        //    if (listMachineIdles.ContainsKey(_machineIdleId))
        //    {
        //        MachineIdle idleMac = listMachineIdles[_machineIdleId];
        //        for (int i = 0; i < idleMac.ListLevels.Count; i++)
        //        {
        //            if (idlePro.GetCurrentLevel() < idleMac.ListLevels[i].startLevel)
        //            {
        //                f = idleMac.ListLevels[i].factor;
        //                break;
        //            }
                       
        //        }
               
        //    }
           
           
        //    return f;
        //}


        private void onProductionTimerHandler(IdleProduction _idlePro)
        {
            //Debug.Log("onProductionTimerHandler " + _idlePro.Id);
            executeInput();
        }



        protected virtual void executeInput()
        {
            if (InputProc)
            {

                if (InputProc) InputProc.SendUpdate(0);
            }
            else //sadece output varsa direk cikti yapan bir makine demek ki 
            {
                executeOutput();
            }

        }

        protected virtual void executeOutput()
        {
            idlePro.CallbackFromGame(IdleAction.PRODUCTION);
            if (OutputProc)
            {
                OutputProc.SendUpdate(0);
                //if(MoneyAnim) MoneyAnim.MoneyTextAnim(idlePro.ValueTotalMoney.Value.ToString(), transform.position, transform.position + new Vector3(0, 2, 0), 0.3f);
                if(MoneyAnim) MoneyAnim.MoneyTextAnim("0.02f", Vector3.zero, Vector3.zero    + new Vector3(0, 100, 0), 0.3f);
            }
        }


        public override void StartGame()
        {
            timer.Start();

            if (InputProc)
            {
                InputProc.EventProcess.AddListener(onInputProcessorHandler);
                InputProc.SetIntParam("AmountPerProcess", InputAmoutPerProcess);
                InputProc.UnitCount = InputUnitCount;
            }

            if (OutputProc)
            {
                OutputProc.SetIntParam("AmountPerProcess", OutputAmoutPerProcess);
                OutputProc.UnitCount = OutputUnitCount;
            }


            if (WidgetUpgrade)
                WidgetUpgrade.EventTap.AddListener(onWidgetTouch);

            updatedTimerDelay();


            base.StartGame();
        }

        public override void Update()
        {
            base.Update();

            if (timer != null)
            {
                //TimerDelay = Mathf.Clamp(TimerDelay, 0.1f, 10f);
                //timer.Delay = TimerDelay;
                timer.Update();

                if (WidgetUpgrade)
                {
                    if (timer.Delay<0.1f) // delay 0.1 saniye altında iken bar full gorunecek
                    {
                        WidgetUpgrade.SetTimeProgress(0);
                    }
                    else
                    {
                        WidgetUpgrade.SetTimeProgress(timer.Ratio() * 100);
                    }
                }
            }


            if (InputProc)
            {
                InputProc.SetIntParam("AmountPerProcess", InputAmoutPerProcess);
            }

            if (OutputProc)
            {
                OutputProc.SetIntParam("AmountPerProcess", OutputAmoutPerProcess);
            }

            //WidgetUpgrade.gameObject.SetActive(true);

            WidgetUpgrade.SetMode(idlePro.GetAvailableNextLevel());

            updatedTimerDelay();

        }

        protected void onWidgetTouch(RoketGame.Touch _info)
        {
            if (_info.Target == WidgetUpgrade)
            {
                //float newDelay = TimerDelay - ( 0.2f);
                //SetTimerDelay(newDelay);



                PopupUpgrade popup = UICont.Instance.GetPageByClass<PageGame>().OpenPopup<PopupUpgrade>();
                if (popup)
                {
                    Param factorDuration = new Param();
                    factorDuration.IdleValue = idlePro.ValueDuratiom;
                    factorDuration.FactorHelper = FactorDurationExtra;
                    factorDuration.Label = "duration";
                    //factorDuration.NextFactorPrefix = new string[] { "+" };

                    Param factorRevenue = new Param();
                    factorRevenue.IdleValue = idlePro.ValueRevenue;
                    factorRevenue.FactorHelper = FactorRevenueExtra;
                    factorRevenue.Label = "revenue";
                    //factorDuration.NextFactorPrefix = new string[] { "+" };


                    popup.Content.Init(this, new Param[]{ factorDuration, factorRevenue } );
                    //int startLevel = ((int)((float)idlePro.GetCurrentLevel()) % LevelCountInStep);
                    //popup.LevelSlider.SetStartAndEndLevel(startLevel, startLevel + machineIdle.levelCount-1);
                  
                }





            }

            updatedTimerDelay();
        }

     

        private void updatedTimerDelay()
        {
            timer.Delay = (float)idlePro.ValueDuratiom.Value;
            //float speed =( 100 / TimerDelay);
            //float speed = 20 / TimerDelay;
            //float speedRound = Mathf.Round(speed * 100f) / 100f;
            //if (WidgetText) WidgetText.SetText("speed \n" + speedRound);
            if (WidgetText) WidgetText.SetText("duration \n" + timer.Delay);
        }

        private void onInputProcessorHandler(Processor.Info _processorInfo)
        {
            if (_processorInfo.Status == Processor.Status.ON_COMPLETED)
            {
                executeOutput();
            }

        }




        public override void OnTouchTap(RoketGame.Touch _info)
        {
            base.OnTouchTap(_info);
            MobileVibration.Vibrate(MobileVibration.Level.VERYLOW, true);
            //if (InputProc) InputProc.SendUpdate(0);
            executeInput();


        }

        /* IDLE */
        public IdleProduction GetIdlePro()
        {
            return idlePro;
        }

        public int GetLevelCount()
        {
            return LevelCountInStep;
        }

       
    }
}