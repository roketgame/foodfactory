﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;

namespace FoodFactory
{
    public class OutputProcessor : BaseProcessor
    {
        //public GameObject OutputProductPrefab;
        public ProductCategory OutputProduct;
        public float ProductInitForce = 0;
        private Vector3 debugDraw;


        public override void Init()
        {
            base.Init();

        }


        public override void Update()
        {
            base.Update();

        }
        public override void StartGame()
        {
            base.StartGame();
        }

        protected override void execute()
        {
            base.execute();

            for (int i = 0; i < UnitCount * amountPerProcess; i++)
            {
                //if (transform.root.gameObject.name == "Spawner")
                //spawn(true);
                //else
                spawn();
            }

        }

        private void spawn(bool _spawnOnPlatform = false)
        {

            BaseProduct product = ((SceneCont)CoreSceneCont.Instance).CreateProduct(OutputProduct, this.transform.root, getRandomPosInBox());





        }



        private Vector3 getRandomPosInBox()
        {
            Vector3 pos = box.transform.position;
            Vector3 volume = box.size;
            Debug.Log(transform.root.gameObject.name + " .volume " + volume.ToString());
            BaseProduct tempProduct = ((SceneCont)CoreSceneCont.Instance).GetTempProduct(OutputProduct);
            if (tempProduct) volume -= tempProduct.GetSize();
            pos.x += Random.Range(-volume.x / 2, volume.x / 2);
            //pos.y = 
            pos.z += Random.Range(-volume.z / 2, volume.z / 2);
            debugDraw = pos;
            return pos;
        }

        void OnDrawGizmosSelected()
        {
            // Draw a yellow sphere at the transform's position
            Gizmos.color = Color.yellow;
            Gizmos.DrawSphere(debugDraw, 0.02f);
        }
    }
}
