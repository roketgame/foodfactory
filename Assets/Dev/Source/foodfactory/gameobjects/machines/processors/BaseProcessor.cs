﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


namespace FoodFactory
{
    namespace Processor
    {
        public enum Status { ON_STARTED, ON_COMPLETED };

        public struct Info
        {
            public BaseProcessor Processor;
            public Status Status;
            public static Info Create(BaseProcessor _processor, Status _status)
            {
                Info info;
                info.Processor = _processor;
                info.Status = _status;
                return info;
            }
        }
        [System.Serializable]
        public class Event : UnityEvent<Info>
        {
        }



    }
    [RequireComponent(typeof(BoxCollider))]
    public class BaseProcessor : SceneObject, IProcessor
    {

        protected BoxCollider box;
        public Processor.Event EventProcess;
        private Dictionary<string, int> listParamsInt;
        protected int amountPerProcess = 1; //her rprocess'de kac tane urun islenecek?
        public int UnitCount = 1; //1 urun kac birimden olusuyor?
        public override void Init()
        {
            base.Init();
            box = GetComponent<BoxCollider>();
            EventProcess = new Processor.Event();
            listParamsInt = new Dictionary<string, int>();

        }

        public override void StartGame()
        {
            base.StartGame();
        }


        public bool SendUpdate(int _type)
        {
            if (_type == 0)
                execute();
            return true;
        }
        protected virtual void execute()
        {
            EventProcess.Invoke(Processor.Info.Create(this, Processor.Status.ON_STARTED));
        }

        public void SetIntParam(string _paramName, int _value)
        {
            if (listParamsInt.ContainsKey(_paramName))
                listParamsInt[_paramName] = _value;
            else
                listParamsInt.Add(_paramName, _value);

            OnIntParamUpdated(_paramName, _value);
        }

        public int GetIntParam(string _paramName)
        {
            return listParamsInt[_paramName];
        }

        public virtual void OnIntParamUpdated(string _paramName, int _value)
        {
            switch (_paramName)
            {
                case "AmountPerProcess":
                    amountPerProcess = _value;
                    break;

            }
        }
    }
}