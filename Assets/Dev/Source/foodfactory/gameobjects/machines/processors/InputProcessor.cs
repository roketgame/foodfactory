﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;


namespace FoodFactory
{
    public class InputProcessor : BaseProcessor
    {
        private List<BaseProduct> listColl;
        public override void Init()
        {
            base.Init();
            listColl = new List<BaseProduct>();
        }





        public override void Update()
        {
            base.Update();


        }
        public override void StartGame()
        {
            base.StartGame();
        }

        //public override void OnCollisionEnter(Collision collision)
        //{
        //    base.OnCollisionEnter(collision);
        //    BaseProduct product = collision.collider.GetComponent<BaseProduct>();
        //    if (product)
        //        if (!listColl.Contains(product))
        //            listColl.Add(product);
        //}

        public override void OnTriggerEnter(Collider collider)
        {
            base.OnTriggerEnter(collider);

            BaseProduct product = collider.GetComponent<BaseProduct>();
            if (product)
                if (!listColl.Contains(product))
                    listColl.Add(product);

        }

        public override void OnTriggerExit(Collider collider)
        {
            base.OnTriggerEnter(collider);

            BaseProduct product = collider.GetComponent<BaseProduct>();
            if (product)
                if (listColl.Contains(product))
                    listColl.Remove(product);

        }

        protected override void execute()
        {
            base.execute();

            int countOfDestroyProduct = Mathf.Min(amountPerProcess, listColl.Count);
            if (countOfDestroyProduct > 0)
            {
                bool destroyIsValid = false;
                while (!destroyIsValid && listColl.Count > 0)
                {
                    if (destroyAndDispatchProducts(countOfDestroyProduct))
                        destroyIsValid = true;
                }



                if (destroyIsValid) EventProcess.Invoke(Processor.Info.Create(this, Processor.Status.ON_COMPLETED));
            }

        }


        private bool destroyAndDispatchProducts(int _countOfDestroyProduct)
        {
            bool result = false;
            List<BaseProduct> listWillDestroy = listColl.GetRange(0, _countOfDestroyProduct);
            foreach (BaseProduct product in listWillDestroy)
            {
                if (product)
                {
                    if (product.GetIsOnPlatform())
                    //if (!product.jointComp)
                    {
                        result = true;
                        ((SceneCont)CoreSceneCont.Instance).RemoveProduct(product);
                    }
                    //else
                    //{
                    //Debug.Log("error ");
                }

            }

            listColl.RemoveRange(0, _countOfDestroyProduct);
            return result;

        }

    }

}