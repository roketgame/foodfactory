﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;

namespace FoodFactory
{
    public class SceneObject : CoreSceneObject
    {
        private Vector3 velocity;
        private Vector3 prevPos;
        [HideInInspector] public Collider colliderComp;

        public override void Init()
        {
            base.Init();
            colliderComp = GetComponent<Collider>();
        }
        public override void StartGame()
        {
            base.StartGame();
            prevPos = transform.position;

        }
        public override void Update()
        {
            base.Update();
            velocity = (transform.position - prevPos) / Time.deltaTime;
            prevPos = transform.position;

        }

        public Vector3 GetVelocity()
        {

            return velocity;
        }
    }

}