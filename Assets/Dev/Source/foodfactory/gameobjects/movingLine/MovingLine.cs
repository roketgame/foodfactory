﻿using PathCreation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;
using static PathCreation.VertexPath;


namespace FoodFactory
{

    public class MovingLine : BaseCont<MovingLine>, IProduction
    {
        public GameObject PlatformPrefab;
        public float PlatformWidth;
        public float PlatformHeight;
        public float SpeedOffset = 0.00023f;
        public float testTime;
        public CoreSceneObject PathMesh;
        public Renderer MeshRenderer;
        public int MeshTiling = 70;
        private Material meshMat;
        public PathCreator Path;
        public PathCreator PathData;
        private float speed = 1;
        //private BoxCollider boxCollider;
        //private List<GameObject> listItemsOn;
        private List<Platform> listPlatforms;
        private VertexPath pathCont;
        private float platformHeightRatio; // movingline uzunluguna gore height'in 0-1 arasindaki degeri;
        private float totalLength; //tum movingline uzunlugu;
        private IdleProduction idlePro;
        //BaseSceneObject platform;
        public IdleValue idleSpeed;
        public FactorHelper FactorSpeed;
        public int LevelCountInStep = 10;

        public override void Init()
        //void Start()
        {
            base.Init();

           

            //boxCollider = GetComponent<BoxCollider>();
            //path = GetComponentInChildren<PathCreator>();
            //listItemsOn = new List<GameObject>();
            listPlatforms = new List<Platform>();

            //Path.bezierPath = new BezierPath(PathData.path.localPoints, false, PathSpace.xyz);
            pathCont = PathData.path;

            if (PlatformPrefab)
            {
                totalLength = 0;
                {
                    Vector3 prev = pathCont.localPoints[0];
                    Vector3 next;
                    for (int i = 0; i < pathCont.localPoints.Length; i++)
                    {
                        next = pathCont.localPoints[i];
                        totalLength += (next - prev).magnitude;
                        prev = next;
                    }
                }

                float w = PlatformWidth;
                float h = PlatformHeight;
                platformHeightRatio = PlatformHeight / totalLength;
                int count = Mathf.FloorToInt(totalLength / h);
                Platform platform;
                for (int i = 0; i < count; i++)
                {
                    platform = SceneCont.Instance.SpawnItem<Platform>(PlatformPrefab, Vector3.zero, null, transform);

                    platform.SetSize(w, h);
                    listPlatforms.Add(platform);
                    float ratioH = (float)i / (float)count;
                    platform.Ratio = ratioH;
                    updatePlatformMove(platform);

                }


            }




            idlePro = new IdleProduction("Platform", 100, false);
            idlePro.ValueLevel.Value = 1;
            //idlePro.ValueDuratiom.Value = 0;
            idlePro.ValueRevenue.Value = 0;
            idlePro.ValueLevelCost.Value = 10;
            //idlePro.ValueAmount.Value = 0;
            idlePro.ValueTotalMoney.Value = 0;
            idlePro.ValueLevelCost.AddFactor(new IdleValueFactor(0, 100, 2f, 2));
            idlePro.ValueLevel.Event.AddListener(onIdleLevelUpdated);
            //idlePro.productionEvent.AddListener(onProductionTimerHandler);

            idleSpeed = new IdleValue("speed", 1);
            idleSpeed.AddFactor(new IdleValueFactor(0, 100, 1f, 1));
            //idleSpeed.AddFactor(new IdleValueFactor(2, 2, 1f, 1));
            //idleSpeed.AddFactor(new IdleValueFactor(4, 4, 1f, 1));
            //idleSpeed.AddFactor(new IdleValueFactor(6, 6, 2f, 1));
            //idleSpeed.AddFactor(new IdleValueFactor(8, 8, 2f, 1));
            //idleSpeed.AddFactor(new IdleValueFactor(8, 8, 2f, 1));
            //idleSpeed.AddFactor(new IdleValueFactor(10, 10, 3f, 1));
            idleSpeed.Value = 3;
            //idleSpeed.Value = 30;
            idlePro.AddCustomIdleValue(idleSpeed);


            FactorSpeed = new FactorHelper("platformSpeed", idleSpeed, 1, LevelCountInStep, 1, LevelCountInStep,  1, 1, 1, false);
            speed = (float)idleSpeed.Value;
          
        }

        /* idlecontroller her level updatedi yaptiktan sonra */
        private void onIdleLevelUpdated(IdleValue _level)
        {
            Debug.Log(name + " new level " + _level.Value);
            Debug.Log(name + " new level idleSpeed " + idleSpeed.Value);
            speed = (float) idleSpeed.Value;
        }

        public void UpgradeLevel()
        {
            idlePro.CallbackFromGame(IdleAction.NEXT_LEVEL);
        }
        private void updatePlatformMove(Platform _platform)
        {
            float _ratio = _platform.Ratio;
            Vector3 loc = pathCont.GetPointAtTime(_ratio, EndOfPathInstruction.Loop);
            Vector3 dir = pathCont.GetDirection(_ratio, EndOfPathInstruction.Loop);
            Quaternion qua = pathCont.GetRotation(_ratio, EndOfPathInstruction.Loop);
            Vector3 rot = qua.eulerAngles;
            rot.z = 0;

            _platform.transform.position = loc;
            _platform.transform.rotation = Quaternion.Euler(rot);
            _platform.Ratio += getRatioSpeed();

   

        }

     
        private float getRatioSpeed()
        {
            return (speed * 0.00023f);
        }

        public override void Update()
        {
            if (GetStatus() != GameStatus.STARTGAME) return;


            foreach (Platform platform in listPlatforms)
            {
                updatePlatformMove(platform);
            }
            if (MeshRenderer)
            {
                meshMat = MeshRenderer.material;
                if (meshMat)
                {

                    meshMat.mainTextureScale = new Vector2(0, MeshTiling);
                }

            }





            if (meshMat) updateShader(speed * -1);





        }


        public float GetPlatformSpeed()
        {
            float s = 0;

            s = totalLength * getRatioSpeed();
            return s;
        }






        private void updateShader(float _speed)
        {
            if (meshMat)
                meshMat.mainTextureOffset = new Vector2(1, _speed * Time.time);


        }


        //protected void onWidgetTouch(Touch.Tap _info)
        //{
        //    if (_info.Target == WidgetUpgrade)
        //    {
        //        float newSpeed = SpeedFactor + 1f;
        //        //if (newDelay > 0)
        //        {
        //            SpeedFactor = newSpeed;
        //        }

        //    }

        //    updatedWidget();
        //}




        public Platform Debug_GetRandomPlatform()
        {
            int randomIndex = Random.Range(0, listPlatforms.Count - 1);
            return listPlatforms[randomIndex];
        }

        public IdleProduction GetIdlePro()
        {
            return idlePro;
        }

        public int GetLevelCount()
        {
            return LevelCountInStep;
        }
    }
}