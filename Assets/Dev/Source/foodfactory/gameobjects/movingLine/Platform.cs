﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;

namespace FoodFactory
{
    public class Platform : SceneObject
    {
        public float Ratio;

        private Joint joint;
        private BoxCollider _collider;

        public override void Init()
        {
            base.Init();
            _collider = GetComponent<BoxCollider>();
            joint = GetComponentInChildren<Joint>();

        }


        public void SetSize(float w, float h)
        {
            this.transform.localScale = new Vector3(w, this.transform.localScale.y, h);

        }







        public Vector3 GetSize()
        {
            return _collider.bounds.extents;
        }

        /* y position of top */
        public float GetTopPosition()
        {
            return transform.position.y + GetSize().y;
        }

        public Vector3 GetRandomPos()
        {
            Vector3 r = transform.position;
            if ((BoxCollider)colliderComp)
            {
                r += Utils.GetRandomPositionInBoxCollider((BoxCollider)colliderComp);
                r.y = GetTopPosition();
            }

            return r;
        }

        void OnDrawGizmosSelected()
        {
            // Draw a semitransparent blue cube at the transforms position
            Gizmos.color = Color.cyan;
            Gizmos.DrawCube(((BoxCollider)colliderComp).bounds.center, ((BoxCollider)colliderComp).bounds.extents);
        }


    }

}