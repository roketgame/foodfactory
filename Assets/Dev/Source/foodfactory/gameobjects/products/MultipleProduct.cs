﻿/* MultipleProduct -> icinde birden fazla urun(item) barindiran urun */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FoodFactory
{
   
    public class MultipleProduct : BaseProduct
    {
        public Vector3 ContainerExtents;
        public override void StartGame()
        {
            base.StartGame();

            if (!gameObject.activeSelf) return;



            Transform[] childs = transform.GetComponentsInChildren<Transform>();
            int totalChild = childs.Length;
            //float xSpace = bounds.x / totalChild;
            //float zSpace = bounds.z;
            float xStart = -(ContainerExtents.x );
            float zStart = -(ContainerExtents.z );
            float yStart = 0;

            for (int i = 0; i < totalChild; i++)
            {
                GameObject child = childs[i].gameObject;
                if (child.name != "mesh" && child != this)
                {
                    Vector3 localPos = child.transform.localPosition;
                    localPos.x = Random.Range(xStart, xStart + ContainerExtents.x);
                    //localPos.y = Random.Range(yStart, yStart + extents.y);
                    localPos.z = Random.Range(zStart, zStart + ContainerExtents.z);
                    child.transform.localPosition = localPos;

                }

            }

        }
    }

}
