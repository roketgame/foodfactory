﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;

namespace FoodFactory
{
    public enum ProductCategory { RAW, PEELED, SLICED, FRIED, PACKAGE };


    [RequireComponent(typeof(Collider))]
    public class BaseProduct : SceneObject
    {
        public ProductCategory Category;
        public bool IsLastProduct = false;
        public Vector3[] RandomLocalRotationRange = new Vector3[2]; //wil add to current local rotation
        public Vector3[] RandomLocalScaleRange = new Vector3[2]; //wil add to current local scale
        public Vector3 lastVelocity;
        private Platform JointTo;
        [HideInInspector]  public FixedJoint jointComp;
        private bool isAutoConnectOnCollision;
        [HideInInspector]  public Vector3 SpawnPosition;

        public override void Init()
        {
            base.Init();
            jointComp = GetComponent<FixedJoint>();
            Reset();

        }

        public void Reset()
        {
            //Debug.Log("reset " + name);
            SetEnableTrigger(true);
            isAutoConnectOnCollision = true;
            lastVelocity = Vector3.zero;
            rb.velocity = Vector3.zero;
            JointTo = null;
            RemoveJoint(0);
            SetLayerName("Product");
            transform.position = SpawnPosition;

        }
        public override void Update()
        {
            base.Update();

            if (!enabled) return;


            if (GetIsOnPlatform() && lastVelocity.magnitude > 0 && rb)
            {
                //Debug.Log("on Platform lastVelocity " + lastVelocity);
                rb.velocity = lastVelocity;
            }


        }

        public override void StartGame()
        {
            base.StartGame();
            if (!gameObject.activeSelf) return;
        }


      
        private Quaternion getRandomRotation(Quaternion _quaternion, Vector3[] _ranges)
        {
            Vector3 rot = _quaternion.eulerAngles + getRandomVector(_ranges);
            return Quaternion.Euler(rot);

        }
        private Vector3 getRandomScale(Vector3 _scale, Vector3[] _ranges)
        {
            Vector3 addScale = _scale;
            addScale.Scale(getRandomVector(_ranges));



            return _scale + addScale;

        }


        private Vector3 getRandomVector(Vector3[] _ranges)
        {
            Vector3 min = Vector3.zero;
            Vector3 max = Vector3.zero;
            if (_ranges.Length > 0) min = _ranges[0];
            if (_ranges.Length > 1) max = _ranges[1];
            float x = Random.Range(min.x, max.y);
            float y = Random.Range(min.y, max.y);
            float z = Random.Range(min.z, max.z);
            return new Vector3(x, y, z);
        }
        public override void OnCollisionEnter(Collision collision)
        {
            base.OnCollisionEnter(collision);




            BaseMachine targetMachine = collision.collider.transform.root.gameObject.GetComponent<BaseMachine>();





        }


        public override void OnTriggerEnter(Collider other)
        {
            base.OnTriggerEnter(other);


            if (isAutoConnectOnCollision && !jointComp)
            {


                if (other.transform.parent)
                {
                    Platform platform = other.GetComponent<Platform>();
                    if (platform)
                    {

                        AddJoint(platform);


                    }
                }

            }


        }

        public virtual void SetEnableTrigger(bool _enable)
        {
            colliderComp.isTrigger = _enable;

            //MeshRenderer ren = GetComponentInChildren<MeshRenderer>();
            //ren.material.color = _enable ? Color.green : Color.red;
        }



        public Vector3 GetSize()
        {
            Vector3 size = Vector3.zero;
            if (colliderComp)
                size = colliderComp.bounds.size;
            else if (GetComponent<Collider>())
                size = GetComponent<Collider>().bounds.size;

            return size;
        }

        public bool GetIsOnPlatform()
        {
            bool result = false;
            string[] rayTargetLayers = { "MovingLine" };
            Vector3 rayStart = transform.position;
            rayStart.y += GetSize().y;
            if (Utils.RAYCAST(rayStart, Vector3.down * 1f, rayTargetLayers, true).Count > 0)
            {
                result = true;
            }
            return result;
        }

        public void RemoveFromScene()
        {
            RemoveJoint(0);

            float delay = IsLastProduct ? 1f : 1f;
            if (delay > 0)
                StartCoroutine(removeWithDelay(delay));
            else
                ((SceneCont)CoreSceneCont.Instance).RemoveProduct(this);
        }

        private IEnumerator removeWithDelay(float waitTime)
        {
            yield return new WaitForSeconds(waitTime);
            ((SceneCont)CoreSceneCont.Instance).RemoveProduct(this);
        }






        /*************** JOING ***********************/



        public void AddJoint(Platform _platform)
        {
            if (JointTo == null)
            {
                JointTo = _platform;


                Rigidbody targetRb = _platform.rb;
                if (targetRb && !jointComp)
                {
                    jointComp = gameObject.AddComponent(typeof(FixedJoint)) as FixedJoint;
                    jointComp.connectedBody = targetRb;


                    //if (HasMultipleChildProducts)
                    {
                        transform.rotation = _platform.transform.rotation;
                        Vector3 loc = transform.position;
                        loc.y = _platform.GetTopPosition() + (GetSize().y / 2);
                        transform.position = loc;
                    }

                    //SetRandomLocalRotationAndScale();
                    SetEnableTrigger(false);

                    if (Category == ProductCategory.SLICED)
                        Debug.Log("??????");

                }


            }
        }

        public void RemoveJoint(float _velocityScale = 1)
        {
            if (JointTo != null)
            {
                //if (_willNoReConnect)
                {
                    Destroy(jointComp);
                }
                lastVelocity = JointTo.GetVelocity() * _velocityScale;
                JointTo = null;
                isAutoConnectOnCollision = false;

                //Debug.Log("-joint : " + this.name + " -> " + JointTo + " lastVelocity : " + lastVelocity);
                SetLayerName("ProductOnMovingLine");
            }

        }

        void OnJointBreak(float breakForce)
        {
            //Debug.Log("A joint has just been broken!, force: " + breakForce);
        }







    }

}