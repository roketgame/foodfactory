﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using RoketGame;

namespace FoodFactory
{
    public class Widget : CoreWidget
    {

        public bool IsScreenWidget;

        public override void OnTouchTap(RoketGame.Touch _info)
        {
            base.OnTouchTap(_info);
        }

        public override void StartGame()
        {
            base.StartGame();

            if (IsScreenWidget)
            {
                transform.LookAt(Camera.main.transform);
                Vector3 rot = transform.rotation.eulerAngles;
                rot.y = 0;
                transform.rotation = Quaternion.Euler(rot);
            }


        }

        public override void Update()
        {
            base.Update();


        }

    }
}