﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace FoodFactory
{
    public class UpgradeWidget : Widget
    {
        public ProgressBarCircle CircleBar;
        public GameObject iconInfo;
        public GameObject iconArrow;
        public Color ColorInfoMode;
        public Color ColorUpgradeMode;
        public override void Init()
        {
            base.Init();

        }
        public override void StartGame()
        {
            base.StartGame();
            //SetSelectable(SelectableStatus.DISABLED);
            SetMode(false);

        }


        //public void SetSelectable(SelectableStatus _status)
        //{
        //    if (_status == SelectableStatus.ENABLED)
        //        setMode(true);
        //    else if (_status == SelectableStatus.DISABLED)
        //        setMode(false);

        //    IsListenTouchEvent = (_status != SelectableStatus.DISABLED);
        //}

        public void SetMode(bool _isModeUpgrade)
        {
            iconArrow.SetActive(_isModeUpgrade);
            iconInfo.SetActive(!_isModeUpgrade);

            Color color = _isModeUpgrade ? ColorUpgradeMode : ColorInfoMode;
            CircleBar.bar.color = color;
            iconArrow.GetComponent<Image>().color = color;
            iconInfo.GetComponent<Image>().color = color;
        }
        public void SetTimeProgress(float _ratio)
        {
            base.Update();
            if (CircleBar)
                CircleBar.UpdateValue(_ratio);
        }
    }
}