﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public interface IKinematic
{
    bool Speed { get; set; }
    
}


public interface IProcessor
{
     bool SendUpdate(int _type);
    void SetIntParam(string _paramName, int _value);
    int GetIntParam(string _paramName);
}


public interface IJoint
{
    void Connect(IJoint _other);
    bool GetIsConnected();
}


//public interface ISelectableUI
//{
//    void SetColor(Color _color);
//    void SetSelectable(SelectableStatus _status);
//    SelectableStatus GetSelectable();
//}

public interface IProduction
{
    IdleProduction GetIdlePro();
    int GetLevelCount(); //level count in each step
    //float GetNextFactor(string _id);
    //IdleProduction GetStepCount(); //level count in each step
}