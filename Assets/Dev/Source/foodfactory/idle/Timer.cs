﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using RoketGame;

public class Timer 
{
    public class EventTimer : UnityEvent<int>
    {
    }

    private float currTime;
    private bool isStarted;
    private float _delay;
    public EventTimer Event;

    public float Delay { get => _delay; set => _delay = Mathf.Max(value,0); }

    public  void Init()
    {
        Event = new EventTimer();
    }

    public void Start()
    {
        isStarted = true;
    }



    public void Update()
    {
        if(isStarted && _delay > 0)
        {
            currTime += Time.deltaTime;

            if (currTime >= _delay)
            {
                currTime = 0;
                Event.Invoke(1);
            }
        }

       
    }


    public float  Ratio()
    {
        return currTime / _delay;
    }
}
