﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;

namespace FoodFactory
{
    public class JointBreaker : SceneObject
    {
        public float VelocityScale = 1; //after break
        private bool listenTrigger;
        public override void Init()
        {
            base.Init();

            Collider _collider = GetComponent<Collider>();
            if (_collider)
            {
                listenTrigger = _collider.isTrigger;
            }
        }

        public override void OnTriggerEnter(Collider other)
        {
            base.OnTriggerEnter(other);
            if (listenTrigger)
            {
                BaseProduct product = other.GetComponent<BaseProduct>();
                if (product)
                {
                    product.RemoveJoint(VelocityScale);
                }
            }

        }

        public override void OnCollisionEnter(Collision collision)
        {
            base.OnCollisionEnter(collision);

            if (!listenTrigger)
            {
                BaseProduct product = collision.collider.GetComponent<BaseProduct>();
                if (product)
                {
                    product.RemoveJoint(VelocityScale);
                }
            }
        }
    }
}
