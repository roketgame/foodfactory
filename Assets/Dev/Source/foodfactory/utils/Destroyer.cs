﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;

namespace FoodFactory
{
    public class Destroyer : SceneObject
    {
        public float DelayOfDestroyItem = 2f;
        public bool IsDestroyOnContactEnter;
        public bool IsDestroyOnContactExit;



        public override void OnCollisionEnter(Collision collision)
        {
            base.OnCollisionEnter(collision);


            if (IsDestroyOnContactEnter) removeFromScene(collision.gameObject);

            //Debug.Log("RemoveProduct Destroyer " + collision.gameObject.name);


        }

        public override void OnCollisionExit(Collision collision)
        {
            base.OnCollisionExit(collision);
            if (IsDestroyOnContactExit) removeFromScene(collision.gameObject);

        }

        public override void OnTriggerEnter(Collider other)
        {
            base.OnTriggerEnter(other);
            if (IsDestroyOnContactEnter) removeFromScene(other.gameObject);


        }

        public override void OnTriggerExit(Collider other)
        {
            base.OnTriggerExit(other);
            if (IsDestroyOnContactExit) removeFromScene(other.gameObject);

        }

        private void removeFromScene(GameObject _go)
        {
            if (_go == null) return;
            BaseProduct product = _go.GetComponent<BaseProduct>();

            if (product)
            {
                product.RemoveFromScene();
            }
            else if (_go.GetComponent<CoreSceneObject>())
            {
                ((SceneCont)CoreSceneCont.Instance).DestroyItem(_go.GetComponent<CoreSceneObject>());
            }
        }



    }
}