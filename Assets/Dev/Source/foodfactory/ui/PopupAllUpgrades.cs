﻿using FoodFactory;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using RoketGame;

namespace FoodFactory
{
    public class PopupAllUpgrades : Popup
    {
        public GameObject ItemGameObject;
        public RectTransform Container;
        public List<UpgradeItem> Contents;
       

        public  void InitPopupAllUpgrades()
        {
            base.Init();
            //Content.Init();
            //GameObject ItemGameObject = Container.transform.GetChild(0).gameObject;
            int childCount = Container.transform.childCount;
            for (int i = 0; i < childCount; i++)
            {
                Container.transform.GetChild(i).gameObject.SetActive(false);


            }

            {//set platform

                //if (_index >= Container.transform.childCount > 0) return null;
                UpgradeItem item = Container.transform.GetChild(0).GetComponent<UpgradeItem>();
                item.gameObject.SetActive(true);
                item.TxtName.SetText("Platform");
                item.LevelSlider.NextFactor1.gameObject.SetActive(false);
                item.LevelSlider.NextFactor2.gameObject.SetActive(false);

                MovingLine machine = (MovingLine)((GameCont)CoreGameCont.Instance).GetProductionByName("Platform");
                Param factorSpeed = new Param();
                factorSpeed.IdleValue = machine.idleSpeed;
                factorSpeed.FactorHelper = machine.FactorSpeed;
                factorSpeed.Label = "speed";
                //factorDuration.NextFactorPrefix = new string[] { "+" };

                //factorDuration.NextFactorPrefix = new string[] { "+" };
                item.Init(machine, new Param[] { });

                Contents.Add(item);
            }

            
            CreateItemFromMachine("Peeler", 1);
            CreateItemFromMachine("Slicer", 2);
            CreateItemFromMachine("Fryer", 3);
            CreateItemFromMachine("Packager", 4);

            foreach (UpgradeItem item in Contents)
            {
                //item.Init();
            }
       

            //Container.DetachChildren();
        }

        private UpgradeItem CreateItemFromMachine(string _machineName, int _index)
        {
            //GameObject go = Instantiate(ItemGameObject, Vector3.zero, Quaternion.identity);
            //go.transform.parent = Container.transform;
            //go.transform.localScale = new Vector3(1,1,1);
            //UpgradeItem item = go.GetComponent<UpgradeItem>();
            if (_index >= Container.transform.childCount) return null;
            UpgradeItem item = Container.transform.GetChild(_index).GetComponent<UpgradeItem>();
            item.gameObject.SetActive(true);
            item.TxtName.SetText(_machineName);
            item.LevelSlider.NextFactor1.gameObject.SetActive(false);
            item.LevelSlider.NextFactor2.gameObject.SetActive(false);

            BaseMachine machine = (BaseMachine) ((GameCont)CoreGameCont.Instance).GetProductionByName(_machineName);
            Param factorDuration = new Param();
            factorDuration.IdleValue = machine.GetIdlePro().ValueDuratiom;
            factorDuration.FactorHelper = machine.FactorDurationExtra;
            factorDuration.Label = "duration";
            //factorDuration.NextFactorPrefix = new string[] { "+" };

            Param factorRevenue = new Param();
            factorRevenue.IdleValue = machine.GetIdlePro().ValueRevenue;
            factorRevenue.FactorHelper = machine.FactorRevenueExtra;
            factorRevenue.Label = "revenue";
            //factorDuration.NextFactorPrefix = new string[] { "+" };
            item.Init(machine, new Param[] { factorDuration, factorRevenue });

            Contents.Add(item);
            return item;
        }

        
        public override void Close()
        {
            base.Close();
            foreach (UpgradeItem item in Contents)
            {
                item.Close();
            }
            //Content.Close();
        }
    }

}
