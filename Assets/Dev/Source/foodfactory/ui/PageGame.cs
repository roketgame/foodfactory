﻿using FoodFactory;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using RoketGame;

namespace FoodFactory
{
    public class PageGame : PageUI
    {
        public MoneyBar Money;
        public ButtonUpgrade BtnUpgrade;
        public Button BtnAllUpgrades;
        public TextMeshProUGUI TxtDiamond;
        public override void Init()
        {
            base.Init();
            BtnUpgrade.EventTap.AddListener(btnUpgradeOnTouch);
            BtnAllUpgrades.EventTap.AddListener(btnAllUpgradesOnTouch);


        }
        private void btnUpgradeOnTouch(RoketGame.TouchUI _uiTap)
        {
            MovingLine.Instance.UpgradeLevel();
            //((GameCont)CoreGameCont.Instance).IdleCont.GetProduction("peeler").CallbackFromGame(IdleAction.NEXT_LEVEL);
            updateButtonsStatus();
        }
        private void btnAllUpgradesOnTouch(RoketGame.TouchUI _uiTap)
        {
            PopupAllUpgrades popup = UICont.Instance.GetPageByClass<PageGame>().OpenPopup<PopupAllUpgrades>();
           
        }

        public override void Update()
        {
            base.Update();
            updateButtonsStatus();
        }
        public override void Open()
        {
            base.Open();
            SetMoney("0");
            SetDiamond("0");
            if (BtnUpgrade)
            {
                BtnUpgrade.TxtTitle.SetText("Platform Speed");
            }

            GetPopup<PopupAllUpgrades>().InitPopupAllUpgrades();

        }

        private void updateButtonsStatus()
        {


            if (BtnUpgrade)
            {
                //IdleProduction idlePro = ((GameCont)CoreGameCont.Instance).IdleCont.GetProduction("peeler");
                IdleProduction idlePro = MovingLine.Instance.GetIdlePro();
                IdleValue idleValueSpeed = MovingLine.Instance.idleSpeed;
                float currSpeed = (float)idleValueSpeed.Value;
                float nextSpeed = (float)idleValueSpeed.Value;
                float nextLevelCost = (float)idlePro.ValueLevelCost.Value;
                BtnUpgrade.Money.SetText("$ "+nextLevelCost.ToString());
                BtnUpgrade.TxtTitleSub.SetText(""+ nextSpeed);

                bool isAvaibleNextLevel = idlePro.GetAvailableNextLevel();
                BtnUpgrade.SetSelectable(isAvaibleNextLevel ? SelectableStatus.ENABLED : SelectableStatus.DISABLED);
            }
        }

        public void RefreshMoney()
        {
            IdleController idleCont =  ((GameCont)CoreGameCont.Instance).IdleCont;

            UICont.Instance.GetPageByClass<PageGame>().SetMoney(idleCont.TotalMoney.GetValueLabel());
        }


        public void SetMoney(string _value)
        {
            Money.SetText(_value);
        }
        public void SetDiamond(string _value)
        {
            TxtDiamond.SetText(_value);
        }
    }
}
