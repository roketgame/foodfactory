﻿using FoodFactory;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using RoketGame;

namespace FoodFactory
{
    public class PopupUpgrade : Popup
    {
        public UpgradeItem Content;
       

        public override void Init()
        {
            base.Init();
            Content.Init();
        }

        
        public override void Close()
        {
            base.Close();
            Content.Close();
        }
    }

}
