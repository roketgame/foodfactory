﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using RoketGame;


namespace FoodFactory
{
    public struct Param
    {
        public IdleValue IdleValue;
        public string Label;
        public string[] NextFactorPrefix; //0:onune, 1:sonuna
        public FactorHelper FactorHelper;
    }


    public class UpgradeItem : CoreUI
    {

        public ButtonUpgrade BtnUpgrade;
        public TextMeshProUGUI TxtName;
        public TextMeshProUGUI TxtParam1;
        public TextMeshProUGUI TxtParam2;
        public SliderLevel LevelSlider;
        private IProduction production;
        private Param[] listParams;

        public override void Init()
        {
            base.Init();
            //if (BtnUpgrade) 
                BtnUpgrade.EventTap.AddListener(onTapBtnUpgrade);
        }

        public override void Update()
        {
            base.Update();
            refreshUI();
        }

     
        public void Init( IProduction _production, Param[]  _params)
        {
            production = _production;
            listParams = _params;
            //machine = _machine;
            LevelSlider.SetTotalLevel(production.GetLevelCount());

            refreshUI();
        }
        private void onTapBtnUpgrade(RoketGame.TouchUI tap)
        {
            if (production != null)
            {
                production.GetIdlePro().CallbackFromGame(IdleAction.NEXT_LEVEL);
            }
            refreshUI();
        }

        private void refreshUI()
        {
            if (production != null)
            {
                if(BtnUpgrade)
                {
                    if (production.GetIdlePro().GetAvailableNextLevel())
                        BtnUpgrade.SetSelectable(SelectableStatus.ENABLED);
                    else
                        BtnUpgrade.SetSelectable(SelectableStatus.DISABLED);

                    BtnUpgrade.Money.SetText(production.GetIdlePro().ValueLevelCost.Value.ToString());
                }


                for (int i = 0; i < listParams.Length; i++)
                {
                    Param param = listParams[i];
                    TextMeshProUGUI txt = null;
                    if (i == 0) txt = TxtParam1;
                    if (i == 1) txt = TxtParam2;

                    if(txt)
                    {
                        txt.SetText(param.Label);
                        //TextMeshPro txtSub = txt.transform.GetChild(1).GetComponent<TextMeshProUGUI>();
                        TextMeshProUGUI txtSub = txt.GetComponentsInChildren<TextMeshProUGUI>()[1];
                        txtSub.SetText(param.IdleValue.GetValueLabel());
                        //txtSub.SetText( ((float)(param.IdleValue.Value)).ToString());


                    }


                    if (LevelSlider && param.FactorHelper != null)
                    {
                        float nextFactor = param.FactorHelper.GetNextFactor( (int) (float) production.GetIdlePro().GetCurrentLevel() );
                        string nextFactorStr = nextFactor.ToString();
                        if(param.NextFactorPrefix != null)
                        {
                            if (param.NextFactorPrefix.Length > 0) nextFactorStr = param.NextFactorPrefix[0] + nextFactorStr;
                            if (param.NextFactorPrefix.Length > 1) nextFactorStr = nextFactorStr + param.NextFactorPrefix[1];
                        }
                       
                        LevelSlider.SetNextFactor(i, nextFactorStr);
                    }
                }
              

             


               

                if (LevelSlider)
                {
                    LevelSlider.SetCurrLevel((float)production.GetIdlePro().GetCurrentLevel());
                    //float durationNextMainLevel = machine.GetNextFactor("DurationExtra");
                    //float revenueNextMainLevel = machine.GetNextFactor("RevenueExtra");
                    //LevelSlider.SetNextFactors(durationNextMainLevel, revenueNextMainLevel);
                }
            }
        }

        public  void Close()
        {
            production = null;
        }
    }
}