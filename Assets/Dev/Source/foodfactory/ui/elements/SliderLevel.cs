﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;


namespace FoodFactory
{
    public class SliderLevel : Slider
    {
        //private int startLevel;
        //private int endLevel;
        private int totalLevel;
        private int currLevel;
        public TextMeshProUGUI NextFactor1;
        public TextMeshProUGUI NextFactor2;
        public override void Init()
        {
            base.Init();

        }

        //public void SetStartAndEndLevel(int _start, int _end)
        //{
        //    startLevel = _start;
        //    endLevel = _end;
        //    //totalLevel = (_end - _start) + 1;
        //    updateText();
        //}

        public void SetTotalLevel(int _totalLevel)
        {
            totalLevel = _totalLevel;
            updateText();
        }

        public void SetNextFactor(int _index, string _label)
        {
            TextMeshProUGUI txt = (_index == 0) ? NextFactor1 : NextFactor2;
            if(txt)
            {
                txt.SetText(_label);
            }
        }
        //public void SetCurrLevel(int _level)
        public void SetCurrLevel(float _level)
        {

            currLevel = (int)_level;

            if (totalLevel == 0) return;

            //int totalLevel = (endLevel - startLevel) + 1;
            float ratio = (float)(currLevel % totalLevel) / (float)totalLevel;
            ratio = Mathf.Clamp(ratio, 0, 1);
            Value = ratio;
            updateText();

            //Debug.Log("slider total " + totalLevel + " curr " + currLevel + " ratio  "  + ratio);


        }

        private void updateText()
        {
            int start = ((int)Mathf.Floor((float) currLevel / (float)totalLevel)) * totalLevel ;
            int end = start + totalLevel;
            SetText("Level " + currLevel   + "/" + end);
        }



    }
}