﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using RoketGame;

namespace FoodFactory
{
  


    public class ButtonElement : CoreUI
    {

        [HideInInspector] public Color COLOR_DEFAULT;
        public Color COLOR_HIGHLIGHT;
        public Color COLOR_SELECTED;
        public Color COLOR_DISABLED;
        [HideInInspector] private Image compImage;
        [HideInInspector] private TextMeshProUGUI compText;
        [HideInInspector] private Shapes2D.Shape compShape;

        public override void Init()
        {
            base.Init();

            compImage = GetComponent<Image>();
            compText = GetComponent<TextMeshProUGUI>();
            if (compImage) COLOR_DEFAULT = compImage.color;
            else if (compText) COLOR_DEFAULT = compText.color;

        }

        public override void SetSelectable(SelectableStatus _status)
        {
            base.SetSelectable(_status);
            switch (_status)
            {
                case SelectableStatus.ENABLED:
                    SetColor(COLOR_DEFAULT);
                    break;
                case SelectableStatus.HIGHLIGHT:
                    SetColor(COLOR_HIGHLIGHT);
                    break;
                case SelectableStatus.SELECTED:
                    SetColor(COLOR_SELECTED);
                    break;
                case SelectableStatus.DISABLED:
                    SetColor(COLOR_DISABLED);
                    break;
            }
        }



        public virtual void SetColor(Color _color)
        {
            //if(_color.a > 0)
            {
                if (compImage) compImage.color = _color;
                else if (compText) compText.color = _color;
            }

        }



    }
}
