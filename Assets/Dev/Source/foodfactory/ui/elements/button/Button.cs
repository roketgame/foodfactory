﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;

namespace FoodFactory
{
    public class Button : CoreUI
    {
        public List<ButtonElement> listButtonElements;

        public override void Init()
        {
            base.Init();
            listButtonElements = new List<ButtonElement>();
            foreach (ButtonElement ui in GetComponentsInChildren<ButtonElement>())
                listButtonElements.Add(ui);



        }


        public override void SetSelectable(SelectableStatus _status)
        {
            base.SetSelectable(_status);
            foreach (ButtonElement ui in listButtonElements)
                ui.SetSelectable(_status);



        }



        //private T GetChildComponentByName<T>(string name) where T : Component
        //{
        //    foreach (T component in GetComponentsInChildren<T>(true))
        //    {
        //        if (component.gameObject.name == name)
        //        {
        //            return component;
        //        }
        //    }
        //    return null;
        //}

    }
}

