﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using RoketGame;
using UnityEngine.UI;

namespace FoodFactory
{
    public class MoneyBar : CoreUI
    {
        private TextMeshProUGUI txt;
        private RectTransform icon;
        public override void Init()
        {
            base.Init();
            txt = GetComponent<TextMeshProUGUI>();
            //txt.autoSizeTextContainer = true;
            if (transform.childCount > 0)
                icon = (RectTransform)transform.GetChild(0).transform;


        }

        public void SetText(string _text)
        {
            //txt.autoSizeTextContainer = true;
            txt.SetText(_text);
            Bounds b1 = txt.bounds;

            Bounds b2 = txt.textBounds;
            //Debug.Log(name + "b1 : " + b1.ToString());
            //Debug.Log(transform.parent.name + "b1 : " + b1.ToString());
            //Debug.Log(transform.parent.name + "b2 : " + b2.ToString());

            //GetComponent<RectTransform>().sizeDelta = new Vector2(b2.extents.x * 2, b2.extents.y * 2);
            //if (icon)
            //icon.localPosition = new Vector3(  -( b2.extents.x ), icon.localPosition.y, icon.localPosition.z);
        }
    }
}
