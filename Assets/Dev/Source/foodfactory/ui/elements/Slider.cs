﻿using DigitalRuby.Tween;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using RoketGame;

//public class EventDynamicString : UnityEvent<string>
//{
//}


//[System.Serializable]
//    public class DynamicString 
//{
//    public string CoreValue;
//    public EventDynamicString _event;
//    private string value;

//    DynamicString()
//    {


//    }
//    public void Update(string _key, string _value)
//    {

//        string keyStart = "<";
//        string keyEnd = ">";
//        string key = keyStart + _key + keyEnd;
//        value = CoreValue.Replace(key, _value);
//        Event.Invoke(value);
//    }
//    public EventDynamicString Event
//    {
//        get
//        {
//            if (_event == null)
//            {
//                value = CoreValue;
//                _event = new EventDynamicString();
//            }
//            return _event;
//        }

//    }
//}
namespace FoodFactory
{

    public class Slider : CoreUI
    {
        //public DynamicString Text;
        private float _value = -1;
        private Shapes2D.Shape barProgress;
        private TextMeshProUGUI txt;

        public override void Init()
        {
            base.Init();
            barProgress = GetComponentsInChildren<Shapes2D.Shape>()[1];
            txt = GetComponentInChildren<TextMeshProUGUI>();
            //Text.Event.AddListener(onTextChanged);

        }

        public void SetText(string _text)
        {
            txt.SetText(_text);
        }
        //private void onTextChanged(string _value)
        //{
        //    txt.SetText(_value);
        //}

        public float Value
        {
            get
            {
                return _value;
            }
            set
            {
                if (_value == value) return;
                _value = value;
                if (barProgress)
                {
                    Vector3 scale = ((RectTransform)barProgress.transform).localScale;
                    scale.x = _value;
                    //((RectTransform)barProgress.transform).localScale = scale;
                    if(_value == 0)
                    {
                        setTween(new Vector3[]{ new Vector3(1,1,1), scale });
                    }
                    else
                    {
                        setTween(new Vector3[] { scale });
                    }

                   
                }
            }
        }


        private void setTween(Vector3[] _newScale)
        {

            RectTransform rectTrans = ((RectTransform)barProgress.transform);
            System.Action<ITween<Vector3>> updateSliderScale = (t) =>
            {
                rectTrans.localScale = t.CurrentValue;
            };


            Vector3Tween tw = barProgress.gameObject.Tween(gameObject.GetInstanceID(),   rectTrans.localScale, _newScale[0], 0.2f, TweenScaleFunctions.CubicEaseInOut, updateSliderScale);
            if(_newScale.Length > 1)
            {
                tw.ContinueWith(new Vector3Tween().Setup(rectTrans.localScale, _newScale[1], 0.3f, TweenScaleFunctions.CubicEaseInOut, updateSliderScale));
            }
        }
    }


}