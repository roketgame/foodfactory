﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitalRuby.Tween;
using UnityEngine.SceneManagement;
using TMPro;
using RoketGame;

public class MoneyAnimation : BaseCont<MoneyAnimation>
{
    public GameObject MoneyPrefab;
    public int ArraySize;
    public List<GameObject> MoneyPrefabs = new List<GameObject>();


    void Start()
    {
        for (int i = 0; i < ArraySize; i++) 
        {
            addNewElem(new Vector3(0, 0, 0),i);
        }
    }

    void addNewElem(Vector3 startPos,int index)
    {
        MoneyPrefabs.Add(Instantiate(MoneyPrefab, startPos, Quaternion.identity));
        MoneyPrefabs[index].SetActive(false);


        RectTransform MoneyPrefabRect = MoneyPrefabs[index].GetComponent<RectTransform>();
        RectTransform _parent = this.GetComponent<RectTransform>();
        MoneyPrefabRect.transform.SetParent(_parent);
        MoneyPrefabRect.anchoredPosition = _parent.position;
        MoneyPrefabRect.anchorMin = new Vector2(0.5f, 0.5f);
        MoneyPrefabRect.anchorMax = new Vector2(0.5f, 0.5f);
        MoneyPrefabRect.pivot = new Vector2(0.5f, 0.5f);
        MoneyPrefabRect.localScale = new Vector3(1, 1, 1);
        MoneyPrefabRect.sizeDelta = _parent.rect.size;
        MoneyPrefabRect.localPosition = startPos;
        MoneyPrefabRect.localRotation = Quaternion.identity;
    }

    public void MoneyTextAnim(string text, Vector3 startPosition, Vector3 endPosition, float animTime)
    {
         int index = 0;
         while (MoneyPrefabs[index].activeSelf)
         {
             index += 1;
             if(index >= MoneyPrefabs.Count)
             {
                addNewElem(startPosition,index);
             }
         }

         TextMeshProUGUI _text = MoneyPrefabs[index].GetComponent<TextMeshProUGUI>();
         _text.text = text + " $";

         TweenMove(MoneyPrefabs[index], startPosition, endPosition, animTime);
    }




    private void TweenMove(GameObject _gameObject, Vector3 startPosition, Vector3 endPosition, float animTime)
    {
        System.Action<ITween<Vector3>> updateTextPos = (t) =>
        {
            _gameObject.SetActive(true);
            _gameObject.transform.localPosition = t.CurrentValue;
            TextMeshProUGUI _text = _gameObject.GetComponent<TextMeshProUGUI>();
            _text.color = new Color32((byte)_text.color.r, (byte)_text.color.g, (byte)_text.color.b, (byte)(255 - 255 * t.CurrentProgress));
        };

        System.Action<ITween<Vector3>> textMoveCompleted = (t) =>
        {
            _gameObject.SetActive(false);
            _gameObject.transform.position = startPosition;
        };



        Vector3 startPos = startPosition;
        Vector3 endPos = endPosition;
        startPos.z = endPos.z = 0.0f;


        _gameObject.gameObject.Tween(_gameObject.GetInstanceID(), startPos, endPos, animTime, TweenScaleFunctions.SineEaseIn, updateTextPos, textMoveCompleted);
    }

}
