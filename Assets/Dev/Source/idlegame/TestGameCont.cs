using System;
using UnityEngine;

namespace Dev.Source.idlegame
{
    public class TestGameCont : MonoBehaviour
    {
        public static TestGameCont Instance;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }
        
        private void Start()
        {
            // idleController.productionEvent

            IdleProduction machinePeeler = new IdleProduction("peeler", 100, true);
            machinePeeler.ValueDuratiom.Value = 0.75f;
            machinePeeler.ValueRevenue.Value = 10;
            machinePeeler.ValueLevelCost.Value = 10;
            machinePeeler.ValueAmount.Value = 10;
            machinePeeler.ValueTotalMoney.Value = 0;


            machinePeeler.ValueDuratiom.AddFactor(new IdleValueFactor(0, 10, -0.05f, 1)); //0-10 (10 dahil) level arasinda "time' i carpan factoru 
            machinePeeler.ValueDuratiom.AddFactor(new IdleValueFactor(11, 100, -0.06f, 1)); //0-10 (10 dahil) level arasinda "time' i carpan factoru 


            machinePeeler.ValueRevenue.AddFactor(new IdleValueFactor(0, 150, 2f, 2)); //0-15 level arasi  "revenue'(kazanc) un carpan factoru
            
            
            machinePeeler.ValueLevelCost.AddFactor(new IdleValueFactor(0, 3, 2f, 1)); //0-10 level arasi  "levelcost" un carpan factoru
            machinePeeler.ValueLevelCost.AddFactor(new IdleValueFactor(4, 6, 5f, 1)); //0-10 level arasi  "levelcost" un carpan factoru
            machinePeeler.ValueLevelCost.AddFactor(new IdleValueFactor(0, 150, 2f, 2)); //0-10 level arasi  "levelcost" un carpan factoru
            machinePeeler.ValueLevelCost.AddFactor(new IdleValueFactor(10, 10, 1000000, 1)); //0-10 (10 dahil) level arasinda "time' i carpan factoru 



            IdleProduction testMachine = new IdleProduction("test", 15, false);
            testMachine.ValueDuratiom.Value = 0.5f;
            testMachine.ValueRevenue.Value = 0;
            testMachine.ValueLevelCost.Value = 8;
            testMachine.ValueAmount.Value = 10;
            testMachine.ValueTotalMoney.Value = 0;

            testMachine.ValueDuratiom.AddFactor(new IdleValueFactor(0, 10, -0.05f, 0)); //0-10 (10 dahil) level arasinda "time' i carpan factoru 
            testMachine.ValueDuratiom.AddFactor(new IdleValueFactor(0, 5, -0.06f,  1)); //0-10 (10 dahil) level arasinda "time' i carpan factoru 

            testMachine.ValueRevenue.AddFactor(new IdleValueFactor(0, 15, 1.1f, 1)); //0-15 level arasi  "revenue'(kazanc) un carpan factoru
            testMachine.ValueLevelCost.AddFactor(new IdleValueFactor(0, 10, 2f , 1)); //0-10 level arasi  "levelcost" un carpan factoru

            //machinePeeler.ValueTime.GameUpdate(IdleAction.CLICK, 1)
            
            testMachine.productionEvent.AddListener(ProductionTimeFinish);
            machinePeeler.productionEvent.AddListener(ProductionTimeFinish);


            IdleController.Instance.Init(100);
            IdleController.Instance.AddProduction(machinePeeler);
            IdleController.Instance.AddProduction(testMachine);


            IdleController.Instance.StartIdle();
        }

        void ProductionTimeFinish(IdleProduction idleProduction)
        {
            idleProduction.CallbackFromGame(IdleAction.PRODUCTION);
        }
    }
}