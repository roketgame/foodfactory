using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Dev.Source.idlegame
{
    public class TestUICont : MonoBehaviour
    {

        public TextMeshProUGUI totalMoneyText;

        private void Start()
        {
            totalMoneyText.text = "0 $";
            IdleController.Instance.MoneyEvent.AddListener(ChangeMoney);

            // TEST --------------------------
            foreach(var production in IdleController.Instance.productions)
            {
                setLoadingBarSize(production.ValueDuratiom.Value, production._time, production.Id, production.ValueLevelCost.Value, production.ValueRevenue.Value);
                setText(production.ValueDuratiom.Value, production._time, production.Id, production.ValueLevelCost.Value, production.ValueRevenue.Value);
            }

            /*BigFloat n0 = new BigFloat("1.01");

            BigFloat n1 = 0.01;
            BigFloat n2 = 0.001;
            Debug.Log((n1 - n2));

            BigFloat n3 = 1.012;
            BigFloat n4 = 0.003;
            BigFloat a = 1;
            BigFloat n6 = 0.00000401;

            Debug.Log((n6 + n4));
            
            Debug.Log(n1.ToString(3,false) + " --- " + n2 + " " + (n1 - n2) + " " + (n1 + n2));
            Debug.Log(n1.ToRationalString() + " --- " + n2 + " " + (n1 - n2) + " " + (n1 + n2));
            Debug.Log(n1.ToMixString() + " --- " + n2 + " " + (n1 - n2) + " " + (n1 + n2));
            Debug.Log((float)n1 + " --- " + (float)n2 + " " + (float)(n1-n2) + " " + (float)(n1+n2) );*/



        }

        BigFloatTest bigFloatTestObj = new BigFloatTest();

        private void ChangeMoney(BigFloat money)
        {
            totalMoneyText.text = bigFloatTestObj.bigFloatToString(money) + " $";
        }

        public void LevelUp(string id)
        {
            IdleController.Instance.productions.Find(production => production.Id == id)
                .CallbackFromGame(IdleAction.NEXT_LEVEL);


            // TEST ------------------------------
            setText(IdleController.Instance.productions.Find(production => production.Id == id).ValueDuratiom.Value, IdleController.Instance.productions.Find(production => production.Id == id)._time, id, IdleController.Instance.productions.Find(production => production.Id == id).ValueLevelCost.Value, IdleController.Instance.productions.Find(production => production.Id == id).ValueRevenue.Value);

        }

        public void Production(string id)
        {
            IdleController.Instance.productions.Find(production => production.Id == id)
                .CallbackFromGame(IdleAction.PRODUCTION);
        }




        // TEST ----------------------------------------------

        public static TestUICont Instance;
        void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        public RectTransform peelerLoadingImageTransform;
        public RectTransform peelerLoadingBarParent;
        public TextMeshProUGUI peelerCostText;
        public TextMeshProUGUI peelerRevenueText;
        public TextMeshProUGUI peelerTimeText;
        public Button peelerExpandButton;
        

        public RectTransform testMachineLoadingImageTransform;
        public RectTransform testMachineLoadingBarParent;
        public TextMeshProUGUI testMachineCostText;
        public TextMeshProUGUI testMachineRevenueText;
        public TextMeshProUGUI testMachineTimeText;
        public Button testMachineExpandButton;

        public void Update()
        {
            foreach (var production in IdleController.Instance.productions)
            {
                setButtonVisible(production.GetAvailableNextLevel(),production.Id);
                if (production.ValueLevel.Value > 0)
                {
                    switch (production.Id)
                    {
                        case "peeler":
                            peelerTimeText.text = (production.ValueDuratiom.Value - production._time).ToString();
                            setLoadingBarSize(production.ValueDuratiom.Value, production._time, production.Id, production.ValueLevelCost.Value, production.ValueRevenue.Value);

                            break;
                        case "test":
                            testMachineTimeText.text = (production.ValueDuratiom.Value - production._time).ToString();
                            setLoadingBarSize(production.ValueDuratiom.Value, production._time, production.Id, production.ValueLevelCost.Value, production.ValueRevenue.Value);

                            break;
                    }
                }
            }
        }


        float _tempSize;
        float _loadingBarSize;
        public void setLoadingBarSize(BigFloat _time, BigFloat _currtime,string _id, BigFloat _cost, BigFloat _revenue)
        {
           
            switch (_id)
            {
                case "peeler":
                    _tempSize = peelerLoadingBarParent.sizeDelta.x / (float)_time;
                    _loadingBarSize = _tempSize * (float)_currtime;
                    peelerLoadingImageTransform.sizeDelta = new Vector2(_loadingBarSize, peelerLoadingBarParent.sizeDelta.y);
                    break;
                case "test":
                     _tempSize = testMachineLoadingBarParent.sizeDelta.x / (float)_time;
                    _loadingBarSize = _tempSize * (float)_currtime;
                    testMachineLoadingImageTransform.sizeDelta = new Vector2(_loadingBarSize, testMachineLoadingBarParent.sizeDelta.y);
                    break;
            }
           
        }
        public void setText(BigFloat _time, BigFloat _currtime, string _id, BigFloat _cost, BigFloat _revenue)
        {
            switch (_id)
            {
                case "peeler":
                    peelerCostText.text = _cost + "$";
                    peelerRevenueText.text = _revenue + "$";
                    peelerTimeText.text = (_time - _currtime).ToString();
                    break;
                case "test":
                    testMachineCostText.text = _cost + "$";
                    testMachineRevenueText.text = _revenue + "$";
                    testMachineTimeText.text = (_time - _currtime).ToString();
                    break;
            }
        }
        public void setButtonVisible(bool getAvailableNextLevet, string _id)
        {
            switch (_id)
            {
                case "peeler":
                    peelerExpandButton.GetComponent<Button>().interactable = getAvailableNextLevet;
                    break;
                case "test":
                    testMachineExpandButton.GetComponent<Button>().interactable = getAvailableNextLevet;
                    break;
            }

        }

    }
}