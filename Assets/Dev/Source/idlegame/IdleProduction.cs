﻿using System;
using System.Collections;
using System.Collections.Generic;
using Dev.Source.idlegame;
using UnityEngine;
using UnityEngine.Events;

public class IdleValue
{
    public string Id;
    public BigFloat Value;
    public Dictionary<int, float[]> Factors;
    public BigFloat InitValue;

    public EventUpdate Event = new EventUpdate();

    private BigFloatTest bigFloatTest = new BigFloatTest();


    public class EventUpdate : UnityEvent<IdleValue>
    {
    }

    public IdleValue(string _id, BigFloat _initValue)
    {
        Id = _id;
        Value = _initValue;
        InitValue = _initValue;
        Factors = new Dictionary<int, float[]>();


        if (Value is float)
        {
            //float ve int durumuna gore bir seyler yapilabilir...
        }
    }

    public void Update(BigFloat newValue)
    {
        Value = newValue;
        Event.Invoke(this);
    }

    /// <summary>
    /// ***** IDLEMATH DESCRIPTION ***** 
    /// <para>TYPE 0: VALUE = VALUE  </para>
    /// <para>TYPE 1: VALUE = VALUE + FACTOR  </para>
    /// <para>TYPE 2: VALUE = VALUE * FACTOR  </para>
    /// <para>TYPE 3: VALUE = initValue * PRODUCTION_LEVEL  </para>
    /// <para>TYPE 4: VALUE = initValue * FACTOR  </para>
    /// <para>TYPE 5: VALUE = BigFloat.Pow(initValue, Mathf.CeilToInt(FACTOR)) </para>
    /// </summary>
    public void AddFactor(IdleValueFactor _factor)
    {
        for (int i = _factor.StartLevel; i <= _factor.EndLevel; i++)
        {
            if (!Factors.ContainsKey(i))
            {
                Factors.Add(i, new float[4]);
            }

            Factors[i][_factor.Type] = _factor.Factor;
        }
    }

    public float GetFactor(int level, int type)
    {
        float[] factors = Factors[level];
        factors = Factors[level]; //level factor listesini getir
        return factors[type]; //ilgili type'a gore factoru don
    }

    public string GetValueLabel()
    {
        return bigFloatTest.bigFloatToString(Value);
    }
}

public class IdleValueFactor
{
    public int StartLevel;
    public int EndLevel;
    public float Factor;
    public int Type;

    /*
     * start : 10 - start :50
     * _initValue -> 3
     * _factor = 3
     * _type = 0  3,9,27,... (carpim)
     * _type = 1  3,6,9,12...  (carpim, 1.levelin parasi x level)
     * _type = 3  2,5,8,11... (toplama)
     *
     * (a+b) = x
     * (2a*b/100*.1)
     * a = b
     */

    public IdleValueFactor(int _startLevel, int _endLevel, float _factor, int _type = 0)
    {
        StartLevel = _startLevel;
        EndLevel = _endLevel;
        Factor = _factor;
        Type = _type;

        /*
        switch (_type)
        {
            case 0:
                //Factor = math.pow(initValue,_factor)
                break;
            case 1: 
                //Factor = initValue * currLevel
                break;
            case 3:
                //Factor = initValue + (currLevel * _factor)
                break;
        }
        */
    }
}


public class IdleProduction
{
    public string Id;
    public float _time;

    private IdleMath idleMath = new IdleMath();

    /* const values : production'in calismasi icin myutlaka tanimli olması gereken value'lar */
    public IdleValue ValueDuratiom;
    public IdleValue ValueRevenue; // Her productiondaki sonrasındaki gelir
    public IdleValue ValueLevel;
    private int maxLevel;
    public bool isLastProduction;
    public IdleValue ValueLevelCost;
    public IdleValue ValueAmount;
    public IdleValue ValueTotalMoney;

    public List<IdleValue> CustomValues = new List<IdleValue>();


    public ProductionEvent productionEvent = new ProductionEvent();


    public IdleProduction(string _id, int maxLevel, bool isLastProduction)
    {
        Id = _id;
        ValueLevel = new IdleValue("level", 0);
        ValueDuratiom = new IdleValue("timer", 0);
        ValueRevenue = new IdleValue("productRevenue", 0);
        ValueAmount = new IdleValue("productAmount", 0);
        ValueLevelCost = new IdleValue("levelCost", 0);
        ValueTotalMoney = new IdleValue("totalMoney", 0);
        this.maxLevel = maxLevel;
        this.isLastProduction = isLastProduction;
    }


    public void Update(float timeDelta)
    {
        _time += timeDelta;
        if (_time >= ValueDuratiom.Value)
        {
            _time = 0;
            ProductionTime();
        }

    }


    public void AddCustomIdleValue(IdleValue idleValue)
    {
        CustomValues.Add(idleValue);
    }

    public BigFloat GetIdleValueByLevel(IdleValue idleValue, int level)
    {
        return idleMath.GetValueByLevel(level, idleValue);
    }
    
    public void CallbackFromGame(IdleAction _action)
    {
        switch (_action)
        {
            case IdleAction.NEXT_LEVEL: //next level a tiklanti
                if (GetAvailableNextLevel())
                {
                    ValueLevel.Update(ValueLevel.Value + 1);
                    ValueTotalMoney.Update(-ValueLevelCost.Value);

                    UpdateValue(ValueLevelCost);
                    UpdateValue(ValueRevenue);
                    UpdateValue(ValueDuratiom);

                    foreach (var idleValue in CustomValues)
                    {
                        UpdateValue(idleValue);
                    }
                }
                else
                {
                    Debug.LogError("Para yok para yok");
                }

                break;
            case IdleAction.PRODUCTION: //production oldu! time sonrasi callback

                BigFloat totalRevenue = 0;
                if (this.isLastProduction)
                {
                    foreach (var production in IdleController.Instance.productions)
                    {
                        totalRevenue += production.ValueRevenue.Value;
                    }
                }
                
                ValueTotalMoney.Update(totalRevenue);// yukarıdaki if içerisine alınabilir ;şu an tüm makinalarda çalışıyor, ama sadece last prod = true olan makina doğru parayı gönderiyor, diğerleri 0 gönderiyor
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(_action), _action, null);
        }
    }

    private void UpdateValue(IdleValue idleValue)
    {
        //idleValue.Value = idleValue.Value * idleValue.GetFactor(0, 3);
        idleValue.Update(idleMath.GetValueByLevel(ValueLevel.Value, idleValue));
        //IdleValueFactor<T> idleValueFactor = idleValue.Factors[ValueLevel.Value + 1];
    }

/*

private void CalculatorOld(IdleValue<float> idleValue)
{
    switch (idleValueFactor.Type)
    {
        case 0:
            //Factor = math.pow(initValue,_factor)
            idleValue.Value = idleValue.Value * idleValue.GetFactor(0, 3);
            break;
        case 1:
            //Factor = initValue * currLevel
            break;
        case 3:
            //Factor = initValue + (currLevel * _factor)
            break;
    }


    //IdleValueFactor<T> idleValueFactor = idleValue.Factors[ValueLevel.Value + 1];
}

*/

    private void ProductionTime()
    {
        productionEvent.Invoke(this);
    }

    public BigFloat GetCurrentLevel()
    {
        return ValueLevel.Value;
    }

/* bir sonraki level'a gecebiliyoz mu? */
    public bool GetAvailableNextLevel()
    {
        return ValueLevelCost.Value <= IdleController.Instance.TotalMoney.Value;
    }
}