using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Dev.Source.idlegame
{
    public class IdleMath
    {
        public BigFloat GetValueByLevel(BigFloat level, IdleValue idleValue)
        {
            BigFloat result = 0;
            BigFloat _temp = idleValue.Value;
            float[] factorValues = new float[] { };
            foreach (var factor in idleValue.Factors)
            {
                if (factor.Key == level)
                {
                    factorValues = factor.Value;

                    for (int i = 0; i < factorValues.Length; i++)
                    {
                        if(CalculateByIdleType(level, i, factorValues[i], _temp, idleValue.InitValue) != 0)
                        {
                            result = CalculateByIdleType(level, i, factorValues[i], _temp, idleValue.InitValue);
                            _temp = result;
                        }
                        
                    }
                }
            }

            return result;
        }


        private BigFloat CalculateByIdleType(BigFloat level, float type, float factor, BigFloat value,
            BigFloat initValue)
        {
            BigFloat _value = 0;
            if (factor != 0)
            {
                switch (type)
                {
                    case 0:
                        _value = value;
                        break;
                    case 1:
                        _value = value + factor;
                        break;
                    case 2:
                        _value = value * factor;
                        break;
                    case 3:
                        _value = initValue * level == 0 ? 1 : level;
                        break;
                    case 4:
                        _value = initValue * factor;
                        break;
                    case 5:
                        _value = BigFloat.Pow(initValue, Mathf.CeilToInt(factor));
                        break;
                }
            }

            return _value;
        }
    }
}