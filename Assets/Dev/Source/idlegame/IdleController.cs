﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum IdleAction
{
    NEXT_LEVEL,
    PRODUCTION
};

public class MoneyEvent : UnityEvent<BigFloat>
{
}

public class ProductionEvent : UnityEvent<IdleProduction>
{
}



public class IdleController : MonoBehaviour
{
    public static IdleController Instance;
    public List<IdleProduction> productions = new List<IdleProduction>();
    public MoneyEvent MoneyEvent = new MoneyEvent();

    public IdleValue TotalMoney = new IdleValue("totalMoney", 0);

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void Init(int totalLevel)
    {
    }

    public void StartIdle()
    {
        foreach (var production in productions)
        {
            //if (production.isLastProduction)
            {
                production.ValueTotalMoney.Event.AddListener(MoneyUpdate);
            }
            
            production.ValueRevenue.Event.AddListener(RevenueUpdate);
            production.ValueLevel.Event.AddListener(LevelUpdate);
        }
    }

    private void LevelUpdate(IdleValue levelValue)
    {
        Debug.Log("Level " + levelValue.Value);
    }

    private void RevenueUpdate(IdleValue revenueValue)
    {
        Debug.Log("RevenueUpdate " + revenueValue.Value);
    }

    private void MoneyUpdate(IdleValue money)
    {
        /*BigFloat totalRevenue = 0;
        foreach (var production in productions)
        {
            totalRevenue += production.ValueRevenue.Value;
        }
        TotalMoney.Value += totalRevenue;*/

  

        TotalMoney.Value += money.Value;

        MoneyEvent.Invoke(TotalMoney.Value); // TEST UI KISMINA GİDİYOR

    }

    public void AddProduction(IdleProduction production)
    {
        productions.Add(production);
    }


    public IdleProduction GetProduction(string _productId)
    {
        IdleProduction p = productions.Find(production => production.Id == _productId);
        return p;
    }


    private void Update()
    {
        foreach (var production in productions)
        {
            if (production.ValueLevel.Value > 0)
            {
                production.Update(Time.deltaTime);
            }
        }
    }

}