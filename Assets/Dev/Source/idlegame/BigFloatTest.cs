﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigFloatTest
{
    
    string[] shortNotation = new string[5] { "", "", "M", "B", "T" };

    void Start()
    {
        BigFloat money = 102.0234;
        float x = 1;

        money = money + x;
        Debug.Log(bigFloatToString(money));
    }


    public string bigFloatToString(BigFloat bigFloatValue)
    {

        if (bigFloatValue > 1000)
        {
            return FormatEveryThirdPower(shortNotation, bigFloatValue);
        }

        BigFloat _tempLeftOver = bigFloatValue % BigFloat.Parse("10");
        string _LeftOver = "";

        if (_tempLeftOver.ToString().Length == 3)
        {

            for (int i = 2; i < 3; i++)
            {
                if (_tempLeftOver.ToString()[i] != 0)
                {
                    _LeftOver = _LeftOver + _tempLeftOver.ToString()[i];
                }
            }
            _LeftOver = "," + _LeftOver;
        }
        else if (_tempLeftOver.ToString().Length > 3)
        {
            for (int i = 2; i < 4; i++)
            {
                _LeftOver = _LeftOver + _tempLeftOver.ToString()[i];
            }
            _LeftOver = "," + _LeftOver;
        }
        return FormatEveryThirdPower(shortNotation, bigFloatValue, _LeftOver);
    }


    public string FormatEveryThirdPower(string[] notations, BigFloat bigFloatValue, string afterTheComma = "")
    {
        BigFloat _bigFloatValue = bigFloatValue;
        int nationValue = 1;
        string notationValue = "";
        BigFloat roundValue = _bigFloatValue - (_bigFloatValue % BigFloat.Parse("1")); //virgülsüz hali

        if (_bigFloatValue >= 1000000) // harflendirmeye 1M'dan başladım
        {
            _bigFloatValue /= 1000;
            nationValue++;
            roundValue = _bigFloatValue - (_bigFloatValue % BigFloat.Parse("1"));
            while (roundValue >= 1000000)
            {
                _bigFloatValue /= 1000;
                nationValue++;
                roundValue = _bigFloatValue - (_bigFloatValue % BigFloat.Parse("1"));
            }
            if (nationValue > notations.Length) return null;
            else notationValue = notations[nationValue];
        }
        string resultValue = "";
        int j = roundValue.ToString().Length - 1;
        for (int i = 0; i < roundValue.ToString().Length; i++)
        {
            if (j % 3 == 2 && j != roundValue.ToString().Length - 1)
            {
                resultValue += ".";
            }
            resultValue = resultValue + roundValue.ToString()[i];
            j--;
        }
        if (bigFloatValue < 1000)
        {
            return resultValue + afterTheComma;
        }

        return resultValue + notationValue;
    }

}
